package zadanie7;

public class MainUser {
    public static void main(String[] args) {
        User user = User.builder()
                .name("Karol")
                .surname("Wisniewski")
                .build();
        System.out.println(user);
    }
}
