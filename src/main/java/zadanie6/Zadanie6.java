package zadanie6;

import java.util.Scanner;

public class Zadanie6 {
    private static final int TIMES = 10;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        calculateFactorialMultiplyTimes(number, TIMES+1);
    }

    private static void calculateFactorialMultiplyTimes(int number, int counter) {
        for (int c = 0; c < counter; c++) {
            int calculatedFactorial = calculateFactorial(number);
            System.out.println(calculatedFactorial);
        }
    }

    private static int calculateFactorial(int n) {
        int result = 1;
        for (int i = 2; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
