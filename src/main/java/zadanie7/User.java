package zadanie7;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class User {
    private String name;
    private String surname;
    private int age;
    private int height;
}

